package main

import (
	"echo-api/app/handler/web"
	"github.com/labstack/echo/v4"
	"net/http"
)

func main() {
	// Echo instance
	e := echo.New()

	// Middleware

	// Route => handler
	e.GET("/ping", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!\n")
	})
	route := web.NewRoutes("/belajar")
	route.RegisterServices(e)

	// Start server
	e.Logger.Fatal(e.Start(":1323"))
}
