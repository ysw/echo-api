package middlewares

import (
	"crypto/subtle"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"strings"
	"time"
)

var IsAuthenticatedBasic = middleware.BasicAuth(func(username, password string, c echo.Context) (bool, error) {
	// Be careful to use constant time comparison to prevent timing attacks
	if subtle.ConstantTimeCompare([]byte(username), []byte("joe")) == 1 &&
		subtle.ConstantTimeCompare([]byte(password), []byte("secret")) == 1 {
		return true, nil
	}
	return false, nil
})

var IsAuthenticated = middleware.JWTWithConfig(middleware.JWTConfig{
	SigningKey: []byte("your-512-bit-secret"),
})

var IsAuthenticatedJWT = middleware.JWTWithConfig(middleware.JWTConfig{
	TokenLookup: "header:token",
	ParseTokenFunc: func(auth string, c echo.Context) (interface{}, error) {
		keyFunc := func(t *jwt.Token) (interface{}, error) {
			if t.Method.Alg() != "HS256" {
				return nil, fmt.Errorf("unexpected jwt signing method=%v", t.Header["alg"])
			}
			signingKey := []byte("your-512-bit-secret")
			return signingKey, nil
		}

		// claims are of type `jwt.MapClaims` when token is created with `jwt.Parse`
		token, err := jwt.Parse(auth, keyFunc)

		if err != nil {
			return nil, err
		}

		if !token.Valid {
			return nil, errors.New("invalid token")
		}
		return token, nil
	},
})

func verifyExpiresAt(m jwt.MapClaims, maxClockSkew int64) bool {
	exp, ok := m["exp"]
	if !ok {
		return false
	}
	var expTime int64
	switch expType := exp.(type) {
	case float64:
		expTime = int64(expType)
	case json.Number:
		expTime, _ = expType.Int64()
	}

	return expTime >= time.Now().Unix() && expTime <= (time.Now().Unix()+maxClockSkew)
}

var IsAuthenticatedToken = middleware.JWTWithConfig(middleware.JWTConfig{

	ParseTokenFunc: func(auth string, c echo.Context) (interface{}, error) {

		authHeader := strings.Split(c.Request().Header.Get("Authorization"), " ")

		tokenString := authHeader[1]

		// claims are of type `jwt.MapClaims` when token is created with `jwt.Parse`
		token, _, err := new(jwt.Parser).ParseUnverified(tokenString, jwt.MapClaims{})

		claims, ok := token.Claims.(jwt.MapClaims)

		if !ok {
			err = fmt.Errorf("invalid claims")
			return err, nil
		}

		keyID := claims["sub"].(string)
		fmt.Println("keyID", keyID)

		if err != nil {
			return nil, err
		}

		if !claims.VerifyIssuedAt(time.Now().Unix(), true) {
			return nil, errors.New("invalid issuedAt time")
		}

		if !verifyExpiresAt(claims, 86400) {
			return nil, errors.New("invalid expiresAt time")
		}

		return token, nil
	},
})
