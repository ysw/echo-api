package web

import (
	"echo-api/app/handler/middlewares"
	"echo-api/app/handler/web/v1"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"time"
)

type Routes struct {
	BaseUrl string
}

func NewRoutes(baseUrl string) *Routes {
	return &Routes{BaseUrl: baseUrl}
}

func (route *Routes) RegisterServices(r *echo.Echo) {

	handler := v1.NewHTTP()
	rGroup := r.Group("/belajar")

	route.RegisterMiddlewares(rGroup)

	// Routes Endpoint
	rGroup.POST("/basic", handler.CreateBasic, middlewares.IsAuthenticatedBasic)
	rGroup.POST("/jwt", handler.CreateJWT, middlewares.IsAuthenticatedJWT)
	rGroup.POST("/token", handler.CreateToken, middlewares.IsAuthenticatedToken)
}

func (route *Routes) RegisterMiddlewares(e *echo.Group) {
	e.Use(middleware.RateLimiter(middleware.NewRateLimiterMemoryStore(2)))
	e.Use(middleware.BodyLimit("2M"))
	e.Use(middleware.TimeoutWithConfig(middleware.TimeoutConfig{
		Timeout: 60 * time.Second,
	}))
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

}
