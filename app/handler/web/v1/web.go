package v1

import (
	"github.com/labstack/echo/v4"
	"net/http"
)

type HTTP struct {
}

func NewHTTP() *HTTP {
	return &HTTP{}
}

func (h *HTTP) CreateBasic(ctx echo.Context) (err error) {
	return ctx.JSON(http.StatusOK, "result")
}

func (h *HTTP) CreateJWT(ctx echo.Context) (err error) {
	return ctx.JSON(http.StatusOK, "result")
}

func (h *HTTP) CreateToken(ctx echo.Context) (err error) {
	return ctx.JSON(http.StatusOK, "result")
}
